// ignore_for_file: prefer_const_constructors

import 'package:bytebank2/database/dao/contato_dao.dart';
import 'package:bytebank2/models/contato.dart';
import 'package:flutter/material.dart';

class FormularioContato extends StatefulWidget {
  const FormularioContato({Key? key}) : super(key: key);

  @override
  State<FormularioContato> createState() => _FormularioContatoState();
}

class _FormularioContatoState extends State<FormularioContato> {
  final TextEditingController _nomeControler = TextEditingController();
  final TextEditingController _contaControler = TextEditingController();
  final ContatoDao _dao = ContatoDao();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Contato'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _nomeControler,
              decoration: InputDecoration(
                labelText: 'Nome completo',
              ),
              style: const TextStyle(fontSize: 24.0),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: TextField(
                controller: _contaControler,
                decoration: InputDecoration(
                  labelText: 'Numero da Conta',
                ),
                style: TextStyle(fontSize: 24.0),
                keyboardType: TextInputType.number,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SizedBox(
                width: double.maxFinite,
                child: ElevatedButton(
                  onPressed: () {
                    final String nome = _nomeControler.text;
                    final int? conta = int.tryParse(_contaControler.text);
                    if (conta != null) {
                      final Contato contato = Contato(0, nome, conta);
                      _dao.salvar(contato).then((id) => Navigator.pop(context));
                    }
                  },
                  child: Text('Criar'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
