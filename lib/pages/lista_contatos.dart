// ignore_for_file: prefer_const_constructors

import 'package:bytebank2/models/contato.dart';
import 'package:bytebank2/pages/formulario_contato.dart';
import 'package:flutter/material.dart';
import 'package:bytebank2/database/dao/contato_dao.dart';

class ListaContatos extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  ListaContatos({Key? key}) : super(key: key);

  @override
  State<ListaContatos> createState() => _ListaContatosState();
}

class _ListaContatosState extends State<ListaContatos> {
  final ContatoDao _dao = ContatoDao();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contatos'),
      ),
      body: FutureBuilder<List<Contato>>(
        initialData: const [],
        future: _dao.buscarTodos(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              break;
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.active:
              break;
            case ConnectionState.done:
              if (snapshot.data != null) {
                final List<Contato>? contatos = snapshot.data;
                return ListView.builder(
                  itemBuilder: (context, indice) {
                    final Contato contato = contatos![indice];
                    return _ContatoItem(contato: contato);
                  },
                  itemCount: contatos!.length,
                );
              }
              return Text('Lista de Contatos Vazia');
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(
                MaterialPageRoute(
                  builder: (context) => FormularioContato(),
                ),
              )
              .then((value) => setState(() {}));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class _ContatoItem extends StatelessWidget {
  final Contato contato;
  const _ContatoItem({Key? key, required this.contato}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          contato.nome,
          style: TextStyle(fontSize: 24.0),
        ),
        subtitle: Text(
          contato.numeroConta.toString(),
          style: TextStyle(fontSize: 16.0),
        ),
      ),
    );
  }
}
