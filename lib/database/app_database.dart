import 'package:bytebank2/database/DAO/Contato_dao.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

Future<Database> criarBanco() async {
  final String path = join(await getDatabasesPath(), 'bytbank.db');
  return openDatabase(path, onCreate: (db, version) {
    db.execute(ContatoDao.sqlTabela);
  }, version: 1);
}
