import 'package:bytebank2/database/app_database.dart';
import 'package:bytebank2/models/contato.dart';
import 'package:sqflite/sqflite.dart';

class ContatoDao {
  static const String _nomeTabela = 'contatos';

  static const String sqlTabela = 'CREATE TABLE $_nomeTabela ('
      'id INTEGER PRIMARY KEY, '
      'nome TEXT,'
      'numero_conta INTEGER)';

  Future<int> salvar(Contato contato) async {
    final Database db = await criarBanco();
    Map<String, dynamic> contatoMap = _toMap(contato);
    return db.insert(_nomeTabela, contatoMap);
  }

  Future<List<Contato>> buscarTodos() async {
    final Database db = await criarBanco();
    final List<Map<String, dynamic>> resultado = await db.query(_nomeTabela);

    List<Contato> contatos = _toList(resultado);
    return contatos;
  }

  Map<String, dynamic> _toMap(Contato contato) {
    final Map<String, dynamic> contatoMap = {};
    contatoMap['nome'] = contato.nome;
    contatoMap['numero_conta'] = contato.numeroConta;
    return contatoMap;
  }

  List<Contato> _toList(List<Map<String, dynamic>> resultado) {
    final List<Contato> contatos = [];
    for (Map<String, dynamic> map in resultado) {
      final Contato contato =
          Contato(map['id'], map['nome'], map['numero_conta']);
      contatos.add(contato);
    }
    return contatos;
  }

  Future<int> apagar(int id) async {
    final Database db = await criarBanco();
    return db.delete(
      _nomeTabela,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> editar(Contato contato) async {
    final Database db = await criarBanco();
    final Map<String, dynamic> contatoMap = _toMap(contato);
    return db.update(
      _nomeTabela,
      contatoMap,
      where: 'id = ?',
      whereArgs: [contato.id],
    );
  }
}
